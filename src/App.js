import "./App.css";
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home/Home";
import Carrier from "./Pages/Carrers/Carrier";
import Portfoliyo from "./Pages/Portfoliyo/Portfoliyo";
import About from "./Pages/About/About";
import Services from "./Pages/Services/Services";
import Contact from "./Pages/Contacts/Contact";
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
// import Spinner from "./Components/Spinner/Spinner";

function App() {
  return (
    <div classNameNameName="App">
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/about' element={<About />} />
        <Route path='/services' element={<Services />} />
        <Route path='/portfoliyo' element={<Portfoliyo />} />
        <Route path='/carrier' element={<Carrier />} />
        <Route path='/contact' element={<Contact />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
